import express from 'express';
import bodyParser from 'body-parser';
import MongoClient from 'mongodb';

var config = {
    mongo: 'mongodb://localhost:27017/webviewos',
    port: 8080
};

function getConfig(name) {
    return process.env[name.toUpperCase()] || config[name];
}

class ApiService {
    constructor() {
        this.app = express();
        this.mongoClientCallback = () => {};
        this.middlewares = [];

        this.app.use(bodyParser.json());
    }

    addMiddleware() {
        this.middlewares.push(arguments);
    }

    setMongoClientCallback(callback) {
        this.mongoClientCallback = callback;
    }

    registerMiddlewares() {
        for (const middleware of this.middlewares) {
            this.app.use(...middleware);
        }
    }

    listen() {
        MongoClient.connect(getConfig('mongo'), (err, db) => {
            this.mongoClientCallback(err, db);

            if (err) throw err;

            this.app.use((req, res, next) => {
                req['db'] = db;
                next();
            });

            this.registerMiddlewares();

            this.app.listen(getConfig('port'));
        });
    }
}

export default ApiService;
